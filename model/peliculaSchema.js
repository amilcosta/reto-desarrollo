const mongoose = require("mongoose");

const PeliculaSchema = new mongoose.Schema({
    _id: {
        type: String,
        required: true,
    },
    Title: {
        type: String
    },
    Year: {
        type: Number
    },
    Released: {
        type: String
    },
    Genre: {
        type: String
    },
    Director: {
        type: String
    },
    Actors: {
        type: String
    },
    Plot: {
        type: String
    },
    Ratings: {
        type: Array
    }
});

const User = mongoose.model("pelicula", PeliculaSchema);

module.exports = User;