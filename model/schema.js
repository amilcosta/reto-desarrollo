const Joi = require('joi');
const validate = require('koa-joi-validate')

/*const schemas = { 
  peliculaGET: validate({
    
    query: Joi.object({
      // URL query Joi validation object
      title: Joi.string()
    })
  }),
  peliculasPOST: Joi.object().keys({
    Title: Joi.string().required(),
    Year: Joi.number().min(4).required(),
    Released: Joi.string(),
    Genre: Joi.string(),
    Director: Joi.string(),
    Actors: Joi.string(),
    Plot: Joi.string().optional,
    Ratings: Joi.any()
  })
  // define all the other schemas below 
}; */


const schemas = validate({
  query: {
    title: Joi.string()
  }
})

module.exports = schemas;