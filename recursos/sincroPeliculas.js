const schemas = require('../model/schema'); 
const middleware = require('../security/joiMiddleware'); 
//const mongoose = require('mongoose');
//const Router  = require('@koa/router');
const peliculaModel = require('../model/peliculaSchema');
const axios = require('axios');

require('dotenv').config();

const respuesta = {
    statusCod: "Ok",
    statusDesc: "",
    data: "",
    page: 1,
    total: 0,
    totalByPage: 0
}

const respuestaPost = {
    statusCod: "Ok",
    statusDesc: "",
    data: ""
}

exports.get = async (ctx) => {

    //middleware(schemas.peliculaGET)    
    //let url = 'http://www.omdbapi.com/?t='+ctx.query.title+'&apikey='+process.env.apiKey;
    let url = 'http://www.omdbapi.com'
    const dataPeli = await axios.get(url,{
        params: {
            t: ctx.query.title,
            apikey: process.env.apiKey,
            y: ctx.query.year
        }
    })


    let data;
    let movie;
    let page = 0;
    let allmovies =0;
    page = ctx.request.header.page != undefined ? ctx.request.header.page  - 1 : 0;
    if(ctx.query.title){
        if(ctx.request.header.year){
            data = await peliculaModel.find({ Title: {$regex: ctx.query.title, $options: 'i'}, Year: ctx.query.year}).sort({_id: 1}).skip(page * 5).limit(5);
            allmovies = await peliculaModel.countDocuments({ Title: {$regex: ctx.query.title, $options: 'i'}, Year: ctx.query.year});
        }else{
            data = await peliculaModel.find({ Title: {$regex: ctx.query.title, $options: 'i'} }).sort({_id: 1}).skip(page * 5).limit(5);
            allmovies = await peliculaModel.countDocuments({ Title: {$regex: ctx.query.title, $options: 'i'}});
        }
        //await peliculaModel.find({ Title: ctx.query.title})
        
        if(data.length==0){
            let total = await peliculaModel.find().sort({_id: -1}).limit(1);
            let idInc = total.length === 0 ? 1 : Number(total[0]._id) + 1;

            movie = new peliculaModel({ _id: idInc, Title: dataPeli.data.Title, Year: dataPeli.data.Year, Released: dataPeli.data.Released, Genre: dataPeli.data.Genre, 
            Director: dataPeli.data.Director, Actors: dataPeli.data.Actors, Plot: dataPeli.data.Plot, Ratings: dataPeli.data.Ratings});
            movie.save();
            allmovies = 1;
            respuesta.totalByPage = allmovies;
        }else{
            page = ctx.request.header.page != undefined ? ctx.request.header.page  - 1 : 0;
            movie = data;
            respuesta.totalByPage = movie.length;
        }
    }else{
        if(ctx.request.header.year){
            movie = await peliculaModel.find({Year: ctx.query.year}).sort({_id: 1}).skip(page * 5).limit(5);
            allmovies = await peliculaModel.countDocuments({Year: ctx.query.year})
        }else{
            movie = await peliculaModel.find().sort({_id: 1}).skip(page * 5).limit(5);
            allmovies = await peliculaModel.count();
        }
        
        respuesta.totalByPage = movie.length;
    }


    respuesta.data = movie;
    respuesta.page = page + 1;
    respuesta.total = allmovies;

    ctx.response.body = respuesta;

}

exports.post = async (ctx) => {
    //console.log("body: ",ctx.request.body," - params: ",ctx.params)
    let movie =ctx.request.body.movie;
    let findMovie = ctx.request.body.find;
    findMovie = findMovie.toUpperCase();
    let replace = ctx.request.body.replace;

    let data;
    let rChange;
    let result;
    if(!movie){
        respuestaPost.statusCod = "WARN";
        respuestaPost.statusDesc = "Se debe enviar un nombre de pelicula";
    }else{
        data = await peliculaModel.find({ Title: {$regex: movie, $options: 'i'}});

        for(let x of data){
            let upperPlot = x.Plot;
            upperPlot = upperPlot.toUpperCase();
            rChange= upperPlot.replace(findMovie, replace.toUpperCase());

            result = await peliculaModel.findOneAndUpdate({_id: x._id},{Plot: rChange});
        }
        
        respuestaPost.data = result;
        
    }
    ctx.response.body = respuestaPost;

}