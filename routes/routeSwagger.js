let Koa = require('koa');
const Router  = require('@koa/router');
const yamljs = require('yamljs');
const koaSwagger = require('koa2-swagger-ui');

// load swagger file.
const spec = yamljs.load('./recursos/peliculas.yaml');
const APIrest = new Router();

//APIrest.get('/docs', koaSwagger({ routePrefix: false, swaggerOptions: { spec } }));
//APIrest.use(koaSwagger({ swaggerOptions: { spec } }));

module.exports = APIrest;