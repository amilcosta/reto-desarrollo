//let Koa = require('koa');
const Router  = require('@koa/router');
const schemas = require('../model/schema'); 
let sincroPeliculas = require('../recursos/sincroPeliculas');


const APIrest = new Router();

APIrest.get('/peliculas',  sincroPeliculas.get);
APIrest.post('/peliculas',sincroPeliculas.post)

module.exports = APIrest;
