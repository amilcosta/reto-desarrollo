let Koa = require('koa');
let Router = require('koa-router');
const mongoose = require('mongoose');
const bodyParser = require('koa-bodyparser');


const indexRouter = require('./routes/index');
const peliculasRouter = require('./routes/routePelicula');
const swaggerRouter = require('./routes/routeSwagger');

const app = new Koa();
//const router =  new Router();

const db = mongoose.connect('mongodb://127.0.0.1:27017/GestionPelicula');

app.use(bodyParser());
app.use(indexRouter.routes());
app.use(indexRouter.allowedMethods());
app.use(peliculasRouter.routes());
app.use(peliculasRouter.allowedMethods());
//app.use(swaggerRouter.routes());
//app.use(swaggerRouter.allowedMethods());

app.listen(3000, function(){
    console.log('Server running on https://localhost:3000')
 });
